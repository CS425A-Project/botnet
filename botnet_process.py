import socket
import atexit
import sys
import filetable_remote.filetable_remote as fm
import json

def cleanup(sock):
	print("Closing current socket...")
	sock.close()
	print("Bye.")

def get_ipv4_addr(host, port):
	addr = socket.getaddrinfo(host, port, socket.AF_INET, 0, socket.SOL_TCP)
	if len(addr) == 0:
		raise Exception("There is no IPv4 address configured for host: "+host)
	return addr[0][-1]

def start_server(filetable_remote, sock_addr):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind(sock_addr)
	s.listen(1024)
	atexit.register(cleanup, s)
	process_requests(filetable_remote, s)

def process_requests(filetable_remote, s):
	while True:
		conn, addr = s.accept()
		raw_data = conn.recv(1024)
		if raw_data != bytes():
			print(raw_data)
			data = json.JSONDecoder().decode(raw_data.decode())
			if 'type' in data.keys():
				if data['type'] == 'insert':
					fm.insert_info(
						filetable_remote,
						data['prev_filename'],
						data['curr_filename'],
						data['ip'],
						data['port']
						)
					conn.send('OK'.encode())
				if data['type'] == 'delete':
					res = fm.del_info(
						filetable_remote,
						data['filename']
						)
					res_encoded = json.JSONEncoder().encode(res)
					conn.send(res_encoded.encode())
				if data['type'] == 'retreive':
					res = fm.get_info(
						filetable_remote,
						data['filename']
						)
					res_encoded = json.JSONEncoder().encode(res)
					conn.send(res_encoded.encode())

def init_filetable_remote(init=False):
	filetable_remote = fm.init_database()
	if init:
		fm.init_table(filetable_remote)
	return filetable_remote

if __name__ == '__main__':
	host= sys.argv[1]
	port = 12347
	if sys.argv[2] == 'init':
		filetable_remote = init_filetable_remote(True)
	elif sys.argv[2] == 'restore':
		filetable_remote = init_filetable_remote(False)
	else:
		print("Invalid arguments.")
		exit(0)

	print("Creating a server with host '" + host + "' and port " + str(port) + "...")
	sock_addr = get_ipv4_addr(host, port)
	start_server(filetable_remote, sock_addr)